default: build

build: GradCheck.java
	javac -cp 'lib/*' GradCheck.java
	mv *class gradcheck

clean:
	rm -rf gradcheck/*

test: build
	java -ea -cp '.:lib/*' gradcheck.GradCheck
