package gradcheck;
import jeigen.*;

class NumUtils {
    /**
     * Helper method for softmax.
     */
    private static boolean normalize(double[] data) {
        double sum = 0;
        for(double x : data) sum += x;
        if(sum == 0) return false;
        for(int i = 0; i < data.length; i++) data[i] /= sum;
        return true;
    }

    /**
     * Normalizes the weights in probs by exponentiating each and then dividing
     * by a constant such that they sum to 1. 
     */
    public static void softmax(double[] probs) {
        double max = Double.NEGATIVE_INFINITY;
        for(int i = 0; i < probs.length; i++)
            max = Math.max(max, probs[i]);
        assert !Double.isInfinite(max);
        double sum = 0;
        for(int i = 0; i < probs.length; i++) {
            probs[i] = Math.exp(probs[i] - max);
            sum += probs[i];
        }
        assert normalize(probs);
    }
}


/**
 * Class used to bundle the parameters for a neural network. It can represent
 * the neural network parameters or the gradients.
 */
class NNetParams {
    public DenseMatrix w1, w2, b;
    public NNetParams(DenseMatrix w1, DenseMatrix w2, DenseMatrix b) {
        this.w1= w1;
        this.w2= w2;
        this.b= b;
    }
}

/**
 * Test class for the problem. Please implement computePredictions and use that to
 * implement computeGradientAnalytical and computeGradientFiniteDiff.
 *
 * Here are some tips for using the DenseMatrix class:
 *
 * Creating a new matrix with r rows and c columns: 
 * DenseMatrix x = DenseMatrix.zeros(r, c);
 *
 * Simple operations: x = y * z
 * x = y * z
 * DenseMatrix x = y.mmul(z);
 *
 * x = y^T
 * DenseMatrix x = y.t();
 *
 * x = y + z
 * DenseMatrix x = y.add(z);
 *
 * x = y .* z (elementwise multiplication)
 * DenseMatrix x = y.mul(z);
 *
 * x = y ^ 2
 * DenseMatrix x = y.pow(2);
 *
 * Getting / setting elements:
 * for (int r = 0; r < x.rows; r++) {
 *     for (int c = 0; c < x.cols; c++) {
 *         x.set(r, c, y.get(r, c));
 *     }
 * }
 */
public class GradCheck {
    // The dimensionality of the input to the neural network.
    public static final int N = 50;
    // The dimensionality of the hidden layer.
    public static final int H = 100;
    // The number of possible output labels.
    public static final int L = 10;
    // The delta for the finite differencing gradient calculation
    public static final double DELTA = 1e-4;
    // The epsilon for the finite differencing gradient calculation
    public static final double EPSILON = 1e-8;

    private static void setSlice(DenseMatrix a, int i, int j, DenseMatrix b) {
        /* Sets the submatrix of a with upper-left corner (i, j) to b. */
        assert i + b.rows <= a.rows;
        assert j + b.cols <= a.cols;
        for (int bi = 0; bi < b.rows; bi++) {
            for (int bj = 0; bj < b.cols; bj++) {
                a.set(i + bi, j + bj, b.get(bi, bj));
            }
        }
    }

    /**
     * Computes the predictions for the neural network. It should return
     * softmax(W2 ( W1 x + b)^3).
     */
    private static double[] computePredictions(NNetParams net, DenseMatrix x) {
        /* THIS PART REMOVED FOR CANDIDATE */
        DenseMatrix h = net.w1.mmul(x).add(net.b).pow(3);
        DenseMatrix predictions = net.w2.mmul(h);
        double[] raw = new double[L];
        for (int l = 0; l < L; l++) raw[l] = predictions.get(l, 0);
        NumUtils.softmax(raw);
        return raw;
        /* UP TO HERE */
    }

    /**
     * Creates and returns a NNetParams object with the analytical gradient with respect
     * to the parameters of the network net, evaluated at input x and output correctLabel.
     */
    private static NNetParams computeGradientAnalytical(NNetParams net, DenseMatrix x, int correctLabel) {
        /* THIS PART REMOVED FOR CANDIDATE */
        double[] predictions = computePredictions(net, x);
        DenseMatrix hPreCube = net.w1.mmul(x).add(net.b);
        DenseMatrix hPostCube = hPreCube.pow(3);

        DenseMatrix w2grad = DenseMatrix.zeros(net.w2.rows, net.w2.cols);
        for (int l = 0; l < predictions.length; l++) {
            double delta = (l == correctLabel ? 1 : 0) - predictions[l];
            DenseMatrix slice = hPostCube.t().mul(delta);
            assert slice.rows == 1 && slice.cols == w2grad.cols;
            setSlice(w2grad, l, 0, slice);
        }

        DenseMatrix hg3 = DenseMatrix.zeros(1, net.w2.cols);
        for (int l = 0; l < net.w2.rows; l++) {
            hg3 = hg3.add(net.w2.row(l).mul((l == correctLabel ? 1 : 0) - predictions[l]));
        }
        assert hg3.rows == 1 && hg3.cols == hPreCube.rows;

        DenseMatrix hgrad = hPreCube.mul(hPreCube).mul(3);
        assert hgrad.rows == hPreCube.rows && hgrad.cols == 1;

        DenseMatrix bgrad = hg3.t().mul(hgrad);
        assert bgrad.rows == net.b.rows && bgrad.cols == net.b.cols;

        DenseMatrix w1grad = bgrad.mmul(x.t());
        assert w1grad.rows == net.w1.rows && w1grad.cols == net.w1.cols;

        return new NNetParams(w1grad, w2grad, bgrad);
        /* UP TO HERE */
    }

    /**
     * Computes the gradient with respect to the parameters of the network net when evaluated on
     * input x and label correctLabel. Unlike the analytical version, this method should use finite
     * differencing to approximate the gradient.
     *
     * Recall that you can evaluate a neural net with packed parameters params by calling
     * computePredictions(unpack(params), x);
     */
    private static NNetParams computeGradientFiniteDiff(NNetParams net, DenseMatrix x, int correctLabel) {
        double[] params = pack(net);
        double[] gradient = new double[params.length];

        /* THIS PART REMOVED FOR CANDIDATE */
        double[] predictions = null;
        for (int i = 0; i < params.length; i++) {
            double orig = params[i];
            params[i] = orig + DELTA;
            predictions = computePredictions(unpack(params), x);
            double upper = Math.log(predictions[correctLabel]);

            params[i] = orig - DELTA;
            predictions = computePredictions(unpack(params), x);
            double lower = Math.log(predictions[correctLabel]);

            gradient[i] = (upper - lower) / (2 * DELTA);

            params[i] = orig;
        }
        /* UP TO HERE */

        return unpack(gradient);
    }

    /** 
     * Convenience method for packing all of net's parameters into one vector.
     */
    private static double[] pack(NNetParams net) {
        int nElts = net.w1.rows * net.w1.cols + net.w2.rows * net.w2.cols + net.b.rows * net.b.cols;
        int ind = 0;
        double[] ret = new double[nElts];
        for (int r = 0; r < net.w1.rows; r++) {
            for (int c = 0; c < net.w1.cols; c++) {
                ret[ind++] = net.w1.get(r, c);
            }
        }
        for (int r = 0; r < net.b.rows; r++) {
            for (int c = 0; c < net.b.cols; c++) {
                ret[ind++] = net.b.get(r, c);
            }
        }
        for (int r = 0; r < net.w2.rows; r++) {
            for (int c = 0; c < net.w2.cols; c++) {
                ret[ind++] = net.w2.get(r, c);
            }
        }
        assert ind == nElts;
        return ret;
    }

    /** 
     * Convenience method for unpacking the neural network parameters in arr with respect to the
     * encoding in pack().
     */
    private static NNetParams unpack(double[] arr) {
        assert arr.length == N * H + H + H * L;
        DenseMatrix w1 = DenseMatrix.zeros(H, N);
        DenseMatrix b = DenseMatrix.zeros(H, 1);
        DenseMatrix w2 = DenseMatrix.zeros(L, H);
        int ind = 0;
        for (int r = 0; r < w1.rows; r++) {
            for (int c = 0; c < w1.cols; c++) {
                w1.set(r, c, arr[ind++]);
            }
        }
        for (int r = 0; r < b.rows; r++) {
            for (int c = 0; c < b.cols; c++) {
                b.set(r, c, arr[ind++]);
            }
        }
        for (int r = 0; r < w2.rows; r++) {
            for (int c = 0; c < w2.cols; c++) {
                w2.set(r, c, arr[ind++]);
            }
        }
        assert ind == arr.length;
        return new NNetParams(w1, w2, b);
    }

    /**
     * Helper method for the test harness. Asserts that a and b are equal within some tolerance.
     */
    private static void assertTol(DenseMatrix a, DenseMatrix b) {
        for (int r = 0; r < a.rows; r++) {
            for (int c = 0; c < a.cols; c++) {
                assert Math.abs(a.get(r, c) - b.get(r, c)) < EPSILON;
            }
        }
    }

    /**
     * Helper method for the test harness. Compares the analytical and finite differencing
     * gradients for a random neural network, input, and label.
     */
    private static void runTest() {
        DenseMatrix w1 = DenseMatrix.rand(H, N);
        DenseMatrix b = DenseMatrix.rand(H, 1);
        DenseMatrix w2 = DenseMatrix.rand(L, H);
        w1 = w1.mul(0.02).sub(0.01);
        b = b.mul(0.02).sub(0.01);
        w2 = w2.mul(0.02).sub(0.01);

        NNetParams net = new NNetParams(w1, w2, b);
        DenseMatrix x = DenseMatrix.rand(N, 1);
        int correctLabel = 5;

        NNetParams analytical = computeGradientAnalytical(net, x, correctLabel);
        NNetParams finiteDiff = computeGradientFiniteDiff(net, x, correctLabel);

        assertTol(analytical.w1, finiteDiff.w1);
        assertTol(analytical.w2, finiteDiff.w2);
        assertTol(analytical.b, finiteDiff.b);
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            runTest();
        }
        System.out.println("All tests passed!");
    }
}
